class Robot:
    version = 1.0

    def __init__(self, nombre):
        self.nombre = nombre
        self.nivel_bateria = 100

    def barrer(self):
        self.nivel_bateria -= 30
        return "Estoy barriendo..."

    def fregar(self):
        self.nivel_bateria = self.nivel_bateria - 50
        return "Agh... debo fregar..."

    def hablar(self):
        self.nivel_bateria -= 15
        print("Bla bla bla bla bla!")

    def cargar(self):
        self.nivel_bateria += 60
        print("Estoy cargado!!")

