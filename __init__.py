from Robot import Robot

if __name__ == "__main__":
    print("LIMPIA CASAS")

    r = input("\nNombre del Robot: ")
    robot = Robot(r)

    while True:
        if robot.nivel_bateria <= 0:
            print("Me quede sin bateria...")
            break

        elif robot.nivel_bateria<20:
            print("me quedare sin bateria regresare a mi base...")

        elif robot.nivel_bateria < 30:
            print("puedo barrer hasta la mitad si quieres")
            print(" pero necesito bateria")

        elif robot.nivel_bateria < 50:
            print("solo puedo fregar a la mitad ")

        print("\n\nMenu - Robot - " + robot.nombre)
        print("1. Barrer")
        print("2. Fregar")
        print("3. Hablar")
        print("4. Ver nivel de bateria")
        print("5. Cargar")
        print("6. Salir")

        try:
            o = int(input("\nOpcion: "))
        except:
            o = -15
        if o == 1:
            print(robot.barrer())
        elif o == 2:
            print(robot.fregar())
        elif o == 3:
            robot.hablar()
        elif o == 4:
            print(robot.nivel_bateria)
        elif o == 5:
            print(robot.cargar())
        elif o == 6:
            print("Chao")
            break
        else:
            print("Opcion invalida")


